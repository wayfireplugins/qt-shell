#include "qt-shell-unstable-v1-protocol.h"

#include "wayfire/core.hpp"
#include "wayfire/object.hpp"
#include <map>
#include <string>
#include <wayfire/util.hpp>
#include <wayfire/view.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/plugin.hpp>

#define QT_SHELL_VERSION    1

struct wf_qt_surface {
    wl_resource             *resource;
    wl_resource             *wl_surface;
    wf::wl_listener_wrapper on_configure;
    wf::wl_listener_wrapper on_destroy;
};

static void handle_destroy( struct wl_client *, struct wl_resource * ) {
}


static void handle_reposition( struct wl_client *, struct wl_resource *, int32_t , int32_t  ) {
}


static void handle_request_activate( struct wl_client *client, struct wl_resource *resource ) {
    auto         surface = static_cast<wf_qt_surface *>(wl_resource_get_user_data( resource ) );
    wayfire_view view    = wf::wl_surface_to_wayfire_view( surface->wl_surface );

    if ( view ) {
        wf::view_focus_request_signal data;
        data.view         = view;
        data.self_request = true;
        view->emit( &data );
        wf::get_core().emit( &data );
    }
}


static void handle_set_size( struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height ) {
}


static void handle_set_minimum_size( struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height ) {
}


static void handle_set_maximum_size( struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height ) {
}


static void handle_set_window_title( struct wl_client *client, struct wl_resource *resource, const char *window_title ) {
}


static void handle_set_window_flags( struct wl_client *client, struct wl_resource *resource, uint32_t flags ) {
}


static void handle_start_system_resize( struct wl_client *client, struct wl_resource *resource, uint32_t serial, uint32_t edge ) {
}


static void handle_start_system_move( struct wl_client *client, struct wl_resource *resource, uint32_t serial ) {
}


static void handle_change_window_state( struct wl_client *client, struct wl_resource *resource, uint32_t state ) {
}


static void handle_raise( struct wl_client *client, struct wl_resource *resource ) {
}


static void handle_lower( struct wl_client *client, struct wl_resource *resource ) {
}


static void handle_ack_configure( struct wl_client *client, struct wl_resource *resource, uint32_t serial ) {
}


static const struct zqt_shell_surface_v1_interface qt_surface_impl = {
    .destroy             = handle_destroy,
    .reposition          = handle_reposition,
    .request_activate    = handle_request_activate,
    .set_size            = handle_set_size,
    .set_minimum_size    = handle_set_minimum_size,
    .set_maximum_size    = handle_set_maximum_size,
    .set_window_title    = handle_set_window_title,
    .set_window_flags    = handle_set_window_flags,
    .start_system_resize = handle_start_system_resize,
    .start_system_move   = handle_start_system_move,
    .change_window_state = handle_change_window_state,
    .raise         = handle_raise,
    .lower         = handle_lower,
    .ack_configure = handle_ack_configure,
};


/**
 * Augments xdg_surface's configure with additional gtk-specific information.
 */
static void handle_xdg_surface_on_configure( wf_qt_surface *surface ) {
    // wayfire_view view = wf::wl_surface_to_wayfire_view( surface->wl_surface );
    //
    // if ( view ) {
    //     send_qt_surface_configure( surface, view );
    // }
}


/**
 * Prevents a race condition where the xdg_surface is destroyed before
 * the gtk_surface's resource and the gtk_surface's destructor tries to
 * disconnect these signals which causes a use-after-free
 */
static void handle_xdg_surface_on_destroy( wf_qt_surface *surface ) {
    surface->on_configure.disconnect();
    surface->on_destroy.disconnect();
}


/**
 * Destroys the gtk_surface object.
 */
static void handle_qt_surface_destroy( wl_resource *resource ) {
    auto surface = static_cast<wf_qt_surface *>(wl_resource_get_user_data( resource ) );

    delete surface;
}


static void handle_qt_shell_surface_create( struct wl_client *client, struct wl_resource *resource, struct wl_resource *surface, uint32_t id ) {
    wf_qt_surface *qt_surface = new wf_qt_surface;

    qt_surface->resource   = wl_resource_create( client, &zqt_shell_surface_v1_interface, wl_resource_get_version( resource ), id );
    qt_surface->wl_surface = surface;
    wl_resource_set_implementation( qt_surface->resource, &qt_surface_impl, qt_surface, handle_qt_surface_destroy );

    wlr_surface     *wlr_surface = wlr_surface_from_resource( surface );

    if ( wlr_xdg_surface *xdg_surface = wlr_xdg_surface_try_from_wlr_surface( wlr_surface ) ) {
        qt_surface->on_configure.set_callback(
            [ = ] (void *) {
                handle_xdg_surface_on_configure( qt_surface );
            }
        );

        qt_surface->on_configure.connect( &xdg_surface->events.configure );

        qt_surface->on_destroy.set_callback(
            [ = ] (void *) {
                handle_xdg_surface_on_destroy( qt_surface );
            }
        );

        qt_surface->on_destroy.connect( &xdg_surface->events.destroy );
    }
}


/**
 * Supported functions of the zqt_shell_v1_interface implementation
 */
static const struct zqt_shell_v1_interface qt_shell_impl = {
    .surface_create = handle_qt_shell_surface_create,
};


/**
 * Destroy the qt_shell object.
 */
static void handle_qt_shell_destroy( wl_resource *resource ) {
    /** Once bound, qt_shell exists as long as the compositor runs. */
}


/**
 * Binds the qt_shell to wayland.
 */
void bind_qt_shell( wl_client *client, void *data, uint32_t version, uint32_t id ) {
    auto resource = wl_resource_create( client, &zqt_shell_v1_interface, QT_SHELL_VERSION, id );

    wl_resource_set_implementation( resource, &qt_shell_impl, data, handle_qt_shell_destroy );
}


class wayfire_qt_shell_impl : public wf::plugin_interface_t {
    public:
        void init() override {
            auto display = wf::get_core().display;
            auto qtshell = wl_global_create( display, &zqt_shell_v1_interface, QT_SHELL_VERSION, NULL, bind_qt_shell );
            if ( not qtshell ) {
                LOGE( "Failed to bind qt-shell." );
            }
        }

        bool is_unloadable() override {
            return false;
        }
};

DECLARE_WAYFIRE_PLUGIN( wayfire_qt_shell_impl );
